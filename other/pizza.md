# Pizza dough

No need to knead a lot

## Intro

-  Prep: 10 mins
-  Wait: couple hours
-  Serves: 4 pizzas
-  Source: random mix of internet

## Ingredients

-  4 cup Plain flour
-  1 tsp Yeast
-  1.5 cup Warm water (NOT boiling or yeast dies)
-  Some salt

## Method

-  Mix flour and salt together
-  Mix yeast in the warm water and dissolve it. Should see some bubbles.
-  Combine together and mix with spoon or hands until combined. Texture should be a bit wet but can make a ball shape.
-  Cover with cling wrap and leave for a few (took mine 4) hours until doubled in size
-  Divide into 4 portions and use 1 each to make a pizza
-  Bake (with toppings on top) for 15 mins 190°C fan force