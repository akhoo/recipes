# Hae bee hiam

## Intro

-  Prep: 10 mins
-  Cooking: 15 mins
-  Source: [domesticgoddess](https://thedomesticgoddesswannabe.com/2017/07/hae-bee-hiam-spicy-dried-shrimp-sambal-2/)

## Ingredients

-  200 g Dried shrimp (or anchovies)
-  15 Dried chilli
-  15 Birds eye chilli
-  6 cloves Garlic
-  3 Shallots
-  1 tsp Salt
-  Oil
-  Water

## Method

-  Soak shrimp in water for 15 mins. Drain then set aside.
-  Cut chillis up to smaller.
-  Blend shallots and garlic together, then add chillis + salt and blend. Use water to make this easier. 
-  Add the shrimp in and blend together. Bit by bit better. 
-  Heat up oil in wok then add the blended mixture. Stir it continuously and cook until mixture becomes dry and dark brown. 
