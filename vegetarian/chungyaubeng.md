# 蔥油餅

## Intro

-  Prep: 10 mins


## Ingredients

-  1 cup Flour
-  Salt
-  Spring onions
-  Oil


## Method

1.  Fry spring onions in some oil then set aside. This is to make the oil have flavour of spring onions, not to brown the spring onions. 
2.  Mix up flour, salt, and some water in a mixing bowl. Add water slowly to not overdo it, mix with chopsticks until it gets a bit sticky then mix with hands. 
3.  Add the spring onions + oil into this mixture and mix until combined. 
4.  Put in some cling wrap and leave in fridge overnight. 
5.  Take out of fridge and divide into 2 portions. Use a roller to make it flat then roll from one side to make a long cylinder. 
6.  Roll the cylinder into a spiral and set aside for 10 minutes. 
7.  Roll it really thin then fry it on generous amount of oil. 


