# Kimchi

## Intro

-  Prep: 2 h
-  Source: internet



## Ingredients

-  1 Medium head (1kg) wombok
-  1/4 cup Salt
-  Water
-  1 tbsp Grated garlic (5 to 6 cloves)
-  1 tsp Grated ginger
-  1 tsp Sugar
-  3 tbsp Fish sauce
-  5 tbsp Gochugaru (korean red chilli flakes)
-  250 g Daikon (white radish) peeled and cut into 2.5cm strips
-  4 Spring onions cut into 2.5 cm strips
-  2 Carrots cut into 2.5 cm strips


## Method

1.  Cut the cabbage lengthwise into quarters and remove the cores. Cut each quarter crosswise into 5 cm wide strips.
2.  Place the cabbage and salt in a large bowl. Mix until it softens, then add water to cover the cabbage. Put a plate on top and weigh it down. Let stand for 1 to 2 hours.
3.  Make the paste: combine the garlic, ginger, sugar, and fish sauce in a small bowl and mix to form a smooth paste. Mix in the gochugaru. 
4.  Rinse the cabbage under cold water and drain in a colander for 15 to 20 minutes. Rinse and dry the bowl you used for salting, and set it aside to use in step 5.
5.  Squeeze any remaining water from the cabbage and return it to the bowl along with the radish, scallions, and seasoning paste.
6.  Mix paste with the vegetables until they are coated.
7.  Pack the kimchi into the jar, pressing down on it until the brine rises to cover the vegetables. Leave at least 2.5 cm  at the top. Seal the jar with the lid.
8.  Let the jar stand at room temperature for 3 to 5 days.
9.  Check the kimchi daily, pressing down on the vegetables to keep them submerged under the brine and release the gas.
10.  When ready refrigerate.
