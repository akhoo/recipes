# Kiam chye (鹹菜)

Pronounced: kiam3chye3 (kiam-ts^{h}ai)

## Intro

-  Prep: 1 day
-  Source: unclephilipsg.blogspot.com



## Ingredients

-  3 Gaai choi (or wombok)
-  1/4 cup Salt
-  Water used to wash rice


## Method

1.  Keep water used to wash rice overnight to start fermentation.
2.  Rup salt into vegetable and leave outside in sun to wilt it.
3.  Put vegetable into water and leave submerged for a few days agitating it daily to remove the bubbles.
4.  Take out and wash and cut then refrigerate. 
