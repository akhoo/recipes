# Bak kua (肉干 or 肉脯)

## Intro

-  Prep: 1 hour
-  Source: Mum

## Ingredients

-  300 g Lean pork mince
-  300 g Poon fei san mince
-  135 g Sugar
-  3 tbsp Fish sauce
-  1 tsp Soy sauce
-  1/4 tsp Salt
-  1/4 tsp Pepper
-  1 tsp Wine


## Method

1.  Mix everything together until gooey then roll out into a thin strip.
2.  Leave in the fridge overnight.
3.  Dry outside under the sun until dry. Takes a couple of hours in the Australian summer.
4.  Cut it up into 10cm squares then fry.


## Tips

Get fly swatters ready for the inevitable onslaught.
