# Hainan chicken rice

## Intro

-  Prep: ?? mins
-  Source: Adam Liaw + wantanmein + nyonyakitchen


## Ingredients

**Chicken**

-  1 large Chicken (about 1.8kg) room temperatured
-  5 cm Ginger
-  2 tsp Salt
-  1 tsp Chicken powder or MSG
-  1 tbsp Sesame oil
-  Coriander
-  Cucumber
-  Tomato

**Sauces**

-  Lazy chilli sauce
    -  6 tbsp thai sweet chilli sauce
    -  2 tsp fresh lemon juice
    -  2 2/3 tbsp sugar
    -  2 tbsp Chicken soup
-  Soy sauce
    -  1 tbsp Shaoxing wine
    -  1 tbsp Sesame oil
    -  2 tbsp Soy sauce
-  Spring onion ginger oil
    -  2 tbsp grated Ginger
    -  Salt
    -  4 Spring onions
    -  1/4 cup Peanut oil

**Rice**

-  3.5 cups Rice
-  1/4 cup Vegetable oil (in total with the rendered oil)
-  4 cloves Garlic
-  3 Shallots

## Method

-  Remove fat from near chickens tail, place in pan and render it under low heat.
-  Place chicken, the green parts of spring onions, 5 cm ginger, salt, chicken powder into 4 L of boiling water. Ensure it tastes savoury and a little salty.
-  Boil it for 45 minutes at very low heat (steaming but not bubbling).
-  Take out the chicken when done (chopstick go in without blood coming out), then place in salted ice water bath for at least 10 minutes turning once.
-  Rub the skin with sesame oil and some salt.
-  Cut the chicken up to serve and pour about half a cup of the soup over the top.
-  Make the sauces (?condiments):
    -  Lazy chilli sauce: mix the things together.
    -  Soy sauce: add these things together.
    -  Spring onion ginger oil: heat oil in pan until almost smoking, then pour into pounded/grated ginger + spring onions. Add salt to taste.
-  Make the rice: pound and cut garlic and shallots, then combine rendered chicken oil with vegetable oil, and fry. Strain oil through a sieve into rice and cook it with the chicken soup.
-  Serve with coriander, cucumber, and tomatoes.



## Tips

Salt ice water bath so it stays flavourful. Don't overcook chicken. Can add fish sauce into soup if it no taste. 
