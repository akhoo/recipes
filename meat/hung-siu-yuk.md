

# 红烧肉 （hung siu yuk）

I prefer mine a bit spicier... titrate to your liking.

## Intro

-  Prep: 20 mins
-  Serves: 6
-  Source: Internet + modified to taste like 契妈的

## Ingredients


-  1000 g Pork belly
-  100 g Ginger
-  1 tbsp Sugar
-  4 Dried chillis
-  3 tsp Sichuan peppercorns
-  1 tbsp Soy sauce
-  1 tbsp Dark soy sauce
-  2 tbsp Shaoxing cooking wine
-  2 Star anise
-  1 small piece Cinnamon bark
-  Water


## Method

1.  Parboil the pork belly and ginger for 5 mins.
2.  Wash off crap, then remove and cut the pork belly into 3x1cm pieces.
3.  Melt sugar in oil then add the sichuan peppercorns, dried chillis and fry for a bit.
4.  Add everything else, add water until the meat is just covered. Simmer for 1.5 hours.
