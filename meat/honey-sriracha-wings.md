

# Honey sriracha wings

Not very spicy wings but they are really sweet. Have to adjust recipe a bit.

## Intro

-  Prep: 20 mins
-  Cooking: 50 mins 200°C
-  Serves: 4
-  Source: Internet

## Ingredients

**Wings**

-  1 kg Chicken wings
-  2 tbsp Melted unsalted butter
-  1 tsp Garlic
-  Salt
-  Pepper
-  Cumin
-  Turmeric
-  Vegetable oil

**Sauce**

-  5 tbsp Unsalted butter
-  1/3 cup Honey
-  1/4 cup Sriracha
-  1 tbsp Soy sauce
-  2 tsp Lime juice


## Method

1.  Marinate the ingredients for the **Wings** together and leave to marinate.
2.  Preheat oven to 200°C.
3.  Bake the wings until browned and crisp (45 to 55 mins).
4.  Mix the ingredients for \textbf{Sauce} together. Dip the wings in the \textbf{Sauce} and serve.
