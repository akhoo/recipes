# Red vinegar pork spare ribs

## Intro

-  Prep: ?? mins
-  Source: Auntie Sonia


## Ingredients

-  2 tbsp & Red vinegar (can use black)
-  4 tbsp & Chinese wine
-  1 block & Rock sugar
-  6 tbsp & Soy sauce (light)
-  1 rack & Pork spare ribs

## Method

-  On low heat boil everything together.
-  Cover and simmer for 1.5 hours.
-  If too thick add water.



## Tips

Never cooked so have no tips at all.
