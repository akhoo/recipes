# Turmeric Chicken

Basically satay minus the peanuts.

## Intro

-  Prep: 20 mins
-  Source: Mum


## Ingredients

-  1 tsp Turmeric
-  4 kg Chicken
-  2 tbsp Coriander
-  2 tsp Fennel
-  2 tsp Cumin
-  2 tbsp Salt
-  12 tbsp Sugar
-  2 tbsp Blended lemon grass
-  4 tbsp Blended onion
-  2 tsp Blended garlic


## Method

1.  Marinate the chicken with everything.
2.  After 10 mins add oil and refrigerate overnight.
3.  Fry


## Tips

Cook it at an outdoor barbeque (e.g. at the park) which are hot and have a much larger surface area than a pan to minimise cooking time
