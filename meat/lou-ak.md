# Lou ak (潮州卤鸭)

Teochew braised duck malaysian style.

## Intro

-  Prep: 1 hour
-  Cooking: 2 hours
-  Source: internet + 大姨

## Ingredients

**Duck**

-  1 Large duck
-  3 tbsp Sea salt
-  1 tbsp Five spice powder
-  2 tbsp Dark soy sauce

**Sauce**

-  40 g White sugar
-  3 Shallots (the small onion thing)
-  5 cloves Garlic
-  3 cm Ginger
-  1 tsp Five spice powder
-  2 Cinnamon sticks
-  2 Star anises
-  4 Cloves
-  1/2 cup Dark soy sauce
-  1/2 cup Light soy sauce
-  Water
-  Salt
-  White pepper


## Extra

-  10 Hardboiled eggs
-  1 Sliced cucumber



## Method

1.  Rub salt over the whole duck and let it marinate for 30+ mins to get rid of some of its odour. Then wash off and drain well.
2.  Rub the black soya sauce and five spice powder all over the duck including the cavity and let it marinate for an hour.
3.  Hardboil the eggs and set aside.
4.  Heat up wok and add sugar till caramelized.
5.  Transfer duck and coat the breast side first with the caramelized sugar and then turn to the other side and repeat till the whole duck is nicely coated with the sugar. Remove from pan when done.
6.  Fry the small onions, spring onions, garlic, ginger.
7.  Add water, soy sauce, white pepper, salt, sugar, cinnamon stick, star anise, cloves, five spice powder, and add the duck.
8.  Bring to boil and lower to medium heat, cover and let it braise for 1 hour. Turn the duck every 30 mins.
9.  Add the hardboiled eggs in. Turn the duck again and braise for another 10 mins.
10.  Let the duck rest for at least an hour or more in the sauce.
11.  Then take it out from the gravy and drain before cutting it into pieces. Transfer them on a serving plate.


## 大姨 version

-  Garlic, small onion, ginger, spring onion
-  Drown with lots of oil
-  Add water
-  Add cleaned duck
-  Add soy sauce, dark soy, white pepper, salt, sugar
-  Add boiled eggs
-  Cover and simmer


## Tips

If a chopstick can stick through the meat then it is good.

Can cook noodles in the sauce after.
