# Sweet potato shephards pie

## Intro

-  Prep: 30 mins
-  Source: That food delivery service

## Ingredients

-  1 Large sweet potato
-  500 g Lamb mince
-  1 cube Chicken stock
-  1 Brown onion
-  60 mL Tomato paste
-  3 piece Celery / 2 Carrots
-  60 g Spinach

## Method

1.  Cut then boil the sweet potato until it is soft. Then mash it up.
2.  Fry the onions, then add the beef and fry for a tiny bit.
3.  Add the tomato paste and chicken stock cube and stir together.
4.  Add the celery.
5.  Turn off the flame and add the spinach in and mix to combine.
6.  Transfer to a brownie baking tray and put the meat mixture in at the bottom, then add the mashed sweet potato to the top.
7.  Bake for 10 mins at 180°C.


## Tips
