# Cha siu (叉燒)

## Intro

-  Source: 契妈

## Ingredients

-  1 Pork neck

**(A)**

-  1 tbsp Red food colouring
-  1 cup Sugar

**(B)**

-  2 tbsp Sesame paste
-  4 tbsp Hoi sin sauce
-  4 tbsp Soy sauce
-  2 tbsp Chinese BBQ sauce
-  4 tbsp Chinese rose wine
-  2 tbsp Minced ginger
-  2 tbsp Minced garlic
-  4 tbsp Ground bean sauce
-  1 tsp Five spice
-  1 tsp Chiu chow chilli oil

**(C)**

-  2: Honey
-  2: Sugar
-  1: Water


## Method

1.  Cut the pork neck into half, then slice each piece into 4 slices. Total 8 slices.
2.  Mix **(A)** then put all 8 pieces of pork into **(A)** and mix them well for at least 1/2 day.
3.  Mix **(B)** well then pir that into (2) and mix them well for oven.
4.  Put the pork neck onto a baking tray and bake for around 10-15 mins at 200°C until the meat turns golden brown.
5.  Brush **(C)**  onto the meat then turn the meat onto the other side and brush with more **(C)**.
6.  Put back in the oven and bake for 15 mins until you think it is golden brown enough. 
