# Tiramisu

## Intro

-  Prep: 30 mins
-  Source: Mum

## Ingredients

-  250 g Mascarpone
-  1 tsp Vanilla
-  3 tbsp Tia maria/marsala
-  150 mL Strong coffee cooled to room temperature
-  120 mL Cream
-  4 tbsp Icing sugar
-  16 Savioardi biscuits


## Method

1.  Whisk mascarpone + vanilla + 50 mL coffee.
2.  Whisk cream and icing sugar until smooth, then fold into mascarpone.
3.  Pour remaining coffee into a bowl. Dip biscuits so they absorb the coffee then line the tin with biscuits.
4.  Layer cream mixture, then biscuits, then cream mixture.


## Tips

Multiply the recipe as necessary.
