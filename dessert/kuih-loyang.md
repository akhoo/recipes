# Kuih loyang

Traditional Malay kuih. Honeycomb biscuits.

## Intro

-  Portion: 40-50 pieces
-  Source: Internet


## Ingredients

-  400 mL Coconut milk
-  200 g All-purpose flour
-  200 g Rice flour
-  2 Large eggs
-  170 g Sugar
-  200 mL Water
-  1/2 tsp Salt
-  Oil for deep frying


1.  Add coconut milk, eggs, sugar, water and salt in a mixing bowl and mix until well combined and all sugar are dissolved.
2.  Sift and all all-purpose flour and rice flour into the mixture. Whisk until well combined with no lumps. If is too thick, add one tablespoon of water at a time until the mixture resembling of a pancake batter.
3.  Heat up oil in a wok/saucepan on medium heat. Tips: dip a wooden chopstick into the hot oil, if the chopstick is bubbling up, it is ready.
4.  Preheat brass moulds in the hot oil, about 2-3 minutes. (The moulds have to be hot enough for batter to cling on them)
5.  CAUTIOUS: Dip hot mould into batter for 10 seconds. Make sure batter coats only the bottom and sides of mould, never over the top.
6.  Slowly lift it up and dip mould back in hot oil. Shake to release from mould and fry until golden brown on both sides.
7.  Take it out from hot oil, and let it cool over paper towel to soak up all the oil.
8.  Repeat until all batter is used up. Store in air-tight containers.
