# Apple slice

Aunty Gina's apple slice. Figure out how hot the oven has to be, polish heritage maybe.


## Intro

-  Prep: 45 mins
-  Baking: 50 mins ?°C
-  Serves: 8-12
-  Source: Aunty Gina


## Ingredients

-  500 g Plain flour
-  200 g Butter
-  2 Eggs
-  1 Egg yolk
-  150 g Caster sugar
-  5-6 tbsp Sour cream
-  5 tsp Baking powder
-  1 kg Granny smith apples (stewed)
-  Sugar
-  Cinnamon


## Method

1.  Sift flour and baking powder.
2.  Soften butter and work into flour.
3.  Add eggs, yolk, cream then work into flour.
4.  Divide pastry into two and roll to suit baking tray.
5.  Bake for 20 mins or until golden.
6.  Peel apples, mix with sugar, and cinnamon then spread on top of pastry.
7.  Roll second pastry on top, prick holes and bake on medium heat for approximately 30 mins.
