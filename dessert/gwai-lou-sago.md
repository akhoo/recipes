# Gwai lou mango sago

"Mango and coconut *tapioca* pudding"

## Intro

-  Prep: 15 mins
-  Source: Woolies

## Ingredients

-  1/2 cup (100 g) Sago
-  400 mL Coconut cream
-  1 tsp Grated lime rind (?)
-  1/4 cup Caster sugar
-  1/4 tsp Salt
-  2 Mangoes (finely chopped)
-  Mango + Coconut to serve
-  1.5 cups Water


## Method

1.  Place sago, coconut cream, lemongrass, vanilla, lime, water in saucepan and stir to combine. Leave 30 mins to soak.
2.  Bring to simmer over medium heat. Reduce to low and cook, stir occasionally, 15 mins or until sago transparent.
3.  Stir in sugar and salt, set aside for 10 mins.
4.  Add mango and stir to combine.
5.  Place in big bowl (and later spoon out) or put in cups and leave for 1 hour to cool completely.


## Tips

Multiply the recipe as necessary. Can go up to 3/4 cup of sago.
