# Mango pudding

An ancient recipe passed down through the ages (cut from some old newspaper magazine). The one you see at yam cha. except better.

## Intro

-  Prep: 30 mins
-  Source: Mum

## Ingredients

-  3 Large mangoes
-  1.5 tbsp Gelatin
-  1/2 cup Cold water
-  1/2 cup Boiling water
-  3/4 cup Sugar
-  3/4 cup Evaporated milk

## Method

1.  Puree mangoes in a food processor.
2.  Soften gelatin in cold water, then add hot water and mix until dissolved. Set aside to cool, then combine with mango puree.
3.  Stir sugar and evaporated milk until sugar dissolves. Add mango mixture.
4.  Pour into a serving bowl and leave overnight.



## Tips

For the cheaters, use mango jelly crystals per packet instructions ;)
