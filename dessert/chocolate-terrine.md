# Chocolate terrine


Super rich chocolate terrine from a certain restaurant. Never made it but making lesser amount.

## Intro

-  Serves lots
-  Source: Musu*

## Ingredients

**(A)**

-  2 kg Butter
-  1.8 kg Chocolate (54% cocao)

**(B)**

-  1.4 kg Sugar
-  40 Eggs

**(C)**

-  180 g Flour


## Method

1.  Put **(A)** in bowl over boiling water and melt.
2.  Mix **(B)** well.
3.  Put **(B)** into **(A)** and mix well.
4.  Add **(C)** into mixture and mix well.
5.  Bake in oven at 85°C for 40 min with hot water bath.
