# Pandan chiffon cake

Donut shaped extremely fluffy cake. Takes lots of skill to make.

## Intro

-  Prep: 40 mins
-  Baking: 40-45 mins 180°C
-  Source: Mum

## Ingredients


**(A)**

-  4 Egg yolks
-  70 g Caster sugar
-  1/4 tsp Salt

**(B)**

-  85 mL Corn oil (1/3 cup)
-  115 mL Pandan juice/essence

**(C)**

-  150 g Cake flour
-  1 tsp Baking powder

**(D)**

-  4 Egg whites
-  70 g Caster sugar
-  1/2 tsp Tartar


## Method

1.  Heat oven 170-180°C.
2.  Sift **(C)** twice then set aside.
3.  Cream **(A)** until sugar is dissolved. Add **(B)** in the order listed. Mix well after each addition.
4.  Add sifted flour and mix well.
5.  In **(D)** beat egg whites until frothy, add tartar then beat until soft peaks. Add sugar then beat until stiff peaks.
6.  Fold 1/2 egg white mixture into yolk mixture.
7.  Pour yolk mixture into white mixture gently.
8.  Pour into pan (donut shaped) and bang against the bench once to release bubbles. Bake for 40-45 mins.
9.  Hang upside balanced on something. 


## Tips

Beating eggs is the most important part.
