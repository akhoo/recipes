# Fruit crumble

Who the fuck needs ice cream with this.

## Intro

-  Prep: 20 mins
-  Baking: 25 mins 180°C
-  Serves: 4
-  Source: Mum


## Ingredients

-  425 g Drained canned fruit (or stewed apples)
-  1/2 cup Plain flour
-  1/2 cup Rolled oats
-  1/4 cup Brown sugar
-  1/2 tsp Cinnamon (optional)
-  3 tbsp Melted butter

## Method

1.  Preheat oven to 180°C.
2.  Mix dry ingredients together.
3.  Add melted butter to the dry mixter and mix well.
4.  Spoon fruit into a 1 L ovenproof dish then top with the crumble mixture.
5.  Bake 25-30 mins.

## Tips

Mum never cooks this amount, always doubles. 
