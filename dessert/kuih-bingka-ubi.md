# Kui bingka ubi

Cassava cake.

## Intro

-  Prep: 20 mins
-  Baking: 35 mins 190°C
-  Source: Mum


## Ingredients

-  500 g Cassava
-  1 cup Sugar
-  1 cup Coconut milk
-  125 g Butter (reduce to 50 g)
-  2 Eggs
-  1/4 tsp Salt
-  1.5 tbsp Corn flour


1.  Mix ingredients into a mixing bowl, then microwave at 1 minute intervals until thick.
2.  Bake at 190°C for 35 minutes in the square baking pan.
